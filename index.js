const express = require('express')
const app = express()
const PORT = process.env.PORT || 8080
const dados = require('./dados.json')

app.get('/personagem', function(req, res){
    const personagem = {
        "nome": dados.nomes[Math.floor(Math.random() * dados.nomes.length)],
        "poder": Math.floor(Math.random() * 5000) + 5001,
        "vivo": Math.random() > 0.5
    }
    res.json(personagem)
})

app.listen(PORT, function(){
    console.log("Servidor iniciado")
})